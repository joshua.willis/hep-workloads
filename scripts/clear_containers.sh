#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

quiet=0
if [ "$1" == "-q" ]; then
  quiet=1
  shift
fi

if [ "$1" == "" ]; then
  CONTAINERS=$(docker ps -a | grep Exit | cut -d" " -f1)
elif [ "$1" == "--all" ]; then
  CONTAINERS=$(docker ps -a | grep -v ^CONTAINER | cut -d" " -f1)
  shift
else
  echo "Usage: $(basename $0) [--all]"
  exit 1
fi

if [ "$quiet" == "0" ]; then
  docker ps -a
  echo ""
fi

if [ "$CONTAINERS" != "" ]; then 
  echo Clearing containers "$CONTAINERS"
  docker rm -f $CONTAINERS
  if [ "$quiet" == "0" ]; then
    echo ""
    docker ps -a
  fi
else
  echo There are no docker containers to clear
fi
